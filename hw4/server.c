/*
 CITAIONS
 http://stackoverflow.com/questions/8611035/proper-fifo-client-server-connection
 This gave me a good understanding of how setup the loops for my program.  This code works by managing the
 fifos in one file/main.

 https://www.youtube.com/watch?v=sP50GHDmfhY
 Video helped me understand pipes and how to read and write using them.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>

#define x 256

int fif();
//void* fif(void *arg);

int main(void){

	//pthread_t thread1;
	//pthread_create(&thread1, NULL, &fif, NULL);

	fif();
}

int fif(void)
//void* fif(void *arg)
{
	puts("This is the server. Client must also be running");

	char str[x];
	int server;
	int client;

	//setup fifos
	mkfifo("Joseph", 0666);
	mkfifo("Salman", 0666);

	server = open("Joseph", O_WRONLY);
	client = open("Salman", O_RDONLY);

	while(1)
	{
		printf("wait for client to send message\n");

		//read client and write
		read(client, str, x);
		printf("Salman(client): %s\n", str);

		//write to client
		printf("You(server): ");
		fgets(str, x, stdin);
		write(server, str, x);
	}



	close(server);
	close(client);

	return EXIT_SUCCESS;
}
