/* I could not figure out how to calculate the time so I kinda wrote code similar to this link
 * http://www.gnu.org/software/libc/manual/html_node/CPU-Time.html

 * */


#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>


#define BUFFERSIZE (128000000u) //     2gb/128mb
//cant write txt file bigger tan 2gb for some reason
#define TOTAL (2000000000u)
char BUFFER [BUFFERSIZE];


int main(int argc, char* argv[]) {
	clock_t start;
	clock_t stop;

    double writtenCount = TOTAL/BUFFERSIZE; // # of times being written
    size_t written = 0;

    int fd = open("HUGEFILE.txt", O_CREAT | O_TRUNC | O_RDWR, 0666); //create a file thats equal to the size of TOTAL

    start = clock();



    /*
    For loop does not work because of compiler version
    error: "for loop initial declaration used outside of c99 mode"
    So I Just used a while loop
     */

    int i =0;
    while(i < writtenCount){

        size_t temp = write(fd, &BUFFER[0], BUFFERSIZE);

        written = written + temp;
        i++;

    }

     stop = clock();

     double executiontime = (double)(stop - start)/CLOCKS_PER_SEC;

    printf("Time:%f seconds\n" , executiontime);
    printf("Written: %zu bytes\n", written);
    printf("Buffer Size: %f bytes\n", (double)BUFFERSIZE);
    printf("Written Count: %f \n ", writtenCount);

return 0;


}




